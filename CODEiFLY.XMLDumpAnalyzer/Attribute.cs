﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODEiFLY.XMLDumpAnalyzer
{
    public class Attribute : DataSample
    {
        public string Name { get; set; }

        public bool CanEmpty { get; set; } = false;

        public bool AlwaysPresent => Node.Occurrences == Occurances;

        public Node Node { get; set; }

        public int Occurances { get; set; }

        
    }
}
