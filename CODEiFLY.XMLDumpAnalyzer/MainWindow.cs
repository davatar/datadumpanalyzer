﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CODEiFLY.XMLDumpAnalyzer
{
    /// <summary>
    /// MainWindow internal logic & parts
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool IsValidNumberInput(string str, int minValue = int.MinValue, int maxValue = int.MaxValue)
        {            
            int intValue;
            return int.TryParse(str, out intValue) && intValue >= minValue && intValue <= maxValue;            
        }

        public void GetPreview(DumpFileProperties _dumpFile, int kiloBytesLimit)
        {
            try
            {
                _previewer.SetContent(_dumpFile.DumpFile, kiloBytesLimit);
                TextPreview.Text = _previewer.OriginalContent;
                XMLPreview.Text = _previewer.AnalyzedContent;
                List<SimpleProperty<PrewiewProperty>> properties = new List<SimpleProperty<PrewiewProperty>>();
                properties.Add(new SimpleProperty<PrewiewProperty>()
                { Key = PrewiewProperty.Encoding, Name = "Detected file encoding", Value = _previewer.DetectedEncoding.HeaderName });
                properties.Add(new SimpleProperty<PrewiewProperty>()
                { Key = PrewiewProperty.FirstTagName, Name = "First TAG name", Value = _previewer.FirstTagName });
                properties.Add(new SimpleProperty<PrewiewProperty>()
                { Key = PrewiewProperty.FirstTagPosition, Name = "First TAG position", Value = _previewer.FirstTagPosition.ToString() });
                properties.Add(new SimpleProperty<PrewiewProperty>()
                { Key = PrewiewProperty.FirstTagCount, Name = "First TAG count", Value = _previewer.FirstTagCount.ToString() });
                properties.Add(new SimpleProperty<PrewiewProperty>()
                { Key = PrewiewProperty.DataHolderPath, Name = "Dataholder TAG path", Value = _previewer.DataHolderPath });
                properties.Add(new SimpleProperty<PrewiewProperty>()
                { Key = PrewiewProperty.DataHolderTagName, Name = "Dataholder TAG name", Value = _previewer.DataHolderTagName });
                properties.Add(new SimpleProperty<PrewiewProperty>()
                { Key = PrewiewProperty.FirstDataHolderTagPosition, Name = "First dataholder TAG position", Value = _previewer.DataHolderTagPosition.ToString() });
                properties.Add(new SimpleProperty<PrewiewProperty>()
                { Key = PrewiewProperty.DataHolderTagCount, Name = "Full dataholder TAG count", Value = _previewer.DataHolderCount.ToString() });
                PreviewProperties.ItemsSource = properties.ToList<SimplePropertyViewModel>();
            }
            catch (Exception ex)
            {
                TextPreview.Text = ex.Message;
            }
        }

        private Node _selectedNode = null;

        private DumpFileProperties _dumpFile = new DumpFileProperties();

        private Previewer _previewer = new Previewer();

        private Analyzer _analyzer = new Analyzer();        

        private const int _defaultKiloBytesLimit = 100;
    }
}
