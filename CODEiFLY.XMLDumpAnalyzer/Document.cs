﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODEiFLY.XMLDumpAnalyzer
{
    public class Document
    {
        public ObservableCollection<Node> Nodes { get; set; } = new ObservableCollection<Node>();

        public string XMLDocumentUri { get; set; }
    }
}
