﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODEiFLY.XMLDumpAnalyzer
{
    /// <summary>
    /// Properties of dump file
    /// </summary>
    public class DumpFileProperties
    {
        public DumpFileProperties(string dumpFile = null)
        {
            DumpFile = dumpFile;
            Properties.Add(new SimpleProperty<DumpFileProperty>() { Key = DumpFileProperty.FileName, Name = "Dump file", Value = dumpFile });
            if (!String.IsNullOrEmpty(dumpFile))
            {
                GetDumpFileProperties();
            }
        }

        public List<SimpleProperty<DumpFileProperty>> Properties { get; } = new List<SimpleProperty<DumpFileProperty>>();

        public ObservableCollection<SimplePropertyViewModel> ViewModel
        {
            get
            {
                var viewModel = new ObservableCollection<SimplePropertyViewModel>();
                foreach (var item in Properties)
                {
                    viewModel.Add(new SimplePropertyViewModel() { Name = item.Name, Value = item.Value });
                }
                return viewModel;
            }
        }

        private void GetDumpFileProperties()
        {
            FileInfo fileInfo = new FileInfo(DumpFile);
            Properties.Add(new SimpleProperty<DumpFileProperty>() { Key = DumpFileProperty.FileSize, Name = "File Size (MB)", Value = (((double)fileInfo.Length / (double)1024) / 1024).ToString("#,##0") });
        }

        public string DumpFile { get; private set; }
    }

}
