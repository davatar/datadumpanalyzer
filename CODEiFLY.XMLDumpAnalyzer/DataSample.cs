﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CODEiFLY.XMLDumpAnalyzer
{
    public class DataSample
    {
        public void AddDataSample(string data)
        {
            if (!_data.Any(x => x == data))
            {
                _data.Add(data);
            }
        }

        public void AddType(string type)
        {
            if (!_types.Any(x => x == type))
            {
                _types.Add(type);
            }
        }

        public string Data
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (string data in _data)
                {
                    sb.Append($" {data},");
                }
                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }
                return sb.ToString();
            }
        }

        public string Types
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (string type in _types)
                {
                    sb.Append($" {type},");
                }
                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }
                return sb.ToString();
            }
        }

        public int ValueCount
        {
            get
            {
                return _data.Count();
            }
        }

        private List<string> _data = new List<string>();

        private List<string> _types = new List<string>();
    }
}
