﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using Microsoft.Win32;
using System.IO;

namespace CODEiFLY.XMLDumpAnalyzer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            PreviewKiloBytesLimit.Text = _defaultKiloBytesLimit.ToString();

            //Document xml = new Document();

            //Node labels = new Node() { Name = "labels", Level = 0 };
            //Node label = new Node() { Name = "label", Level = 1 };
            //label.Attributes.Add(new Attribute() { Name = "A1" });
            //labels.Nodes.Add(label);
            //Node images = new Node() { Name = "images", Level = 2 };
            //label.Nodes.Add(images);
            //Node image = new Node() { Name = "image", Level = 3 };
            //image.Attributes.Add(new Attribute() { Name = "height" });
            //image.Attributes.Add(new Attribute() { Name = "type" });
            //image.Attributes.Add(new Attribute() { Name = "uri" });
            //image.Attributes.Add(new Attribute() { Name = "width" });
            //images.Nodes.Add(image);
            //Node id = new Node() { Name = "id", HasElementValue = true };
            //label.Nodes.Add(id);
            //xml.Nodes.Add(labels);
            //StructureTreeView.ItemsSource = xml.Nodes;

        }

        private void OpenDumpFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "XML Files|*.xml";

            if (openFileDialog.ShowDialog() == true)
            {
                _dumpFile = new DumpFileProperties(openFileDialog.FileName);
                DumpFileProperties.ItemsSource = _dumpFile.ViewModel;
            }
        }

        private void PreviewKiloBytesLimit_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValidNumberInput(((TextBox)sender).Text + e.Text, 1, 10000);
        }

        private void GetPrewiew_Click(object sender, RoutedEventArgs e)
        {
            int kiloBytesLimit = _defaultKiloBytesLimit;
            int.TryParse(PreviewKiloBytesLimit.Text, out kiloBytesLimit);
            GetPreview(_dumpFile, kiloBytesLimit);
            _analyzer.Analyze(_previewer.XMLDocument);
            StructureTreeView.ItemsSource = _analyzer.Document?.Nodes;
        }

        private void StructureTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            _selectedNode = (Node)e.NewValue;
            List<SimpleProperty<NodeProperties>> nodeProperties = new List<SimpleProperty<NodeProperties>>();
            nodeProperties.Add(new SimpleProperty<NodeProperties>() { Key = NodeProperties.Name, Name = "Node name", Value = _selectedNode.Name });
            nodeProperties.Add(new SimpleProperty<NodeProperties>() { Key = NodeProperties.Level, Name = "Node level", Value = _selectedNode.Level.ToString() });
            nodeProperties.Add(new SimpleProperty<NodeProperties>() { Key = NodeProperties.Occurences, Name = "Node occurences", Value = _selectedNode.Occurrences.ToString() });
            nodeProperties.Add(new SimpleProperty<NodeProperties>() { Key = NodeProperties.HasElementValue, Name = "Has element value", Value = _selectedNode.HasElementValue.ToString() });
            nodeProperties.Add(new SimpleProperty<NodeProperties>() { Key = NodeProperties.ValueTypes, Name = "Detected data types", Value = _selectedNode.Types });
            nodeProperties.Add(new SimpleProperty<NodeProperties>() { Key = NodeProperties.ValueSamples, Name = "Detected value count", Value = _selectedNode.ValueCount.ToString() });
            nodeProperties.Add(new SimpleProperty<NodeProperties>() { Key = NodeProperties.ValueSamples, Name = "Detected value", Value = _selectedNode.Data });
            NodePropertiesData.ItemsSource = nodeProperties.ToList<SimplePropertyViewModel>();
            AttributesData.Items.Clear();
            foreach (var attribute in _selectedNode.Attributes)
            {
                AttributesData.Items.Add(attribute.Name);
            }
        }

        private void AttributesData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_selectedNode != null)
            {
                //string 
            }
            //e.AddedItems[0]
        }
    }
}
